import React, { Component } from 'react';

class CurrencyInput extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.handlerChange(event.target.value);
  }

  render() {
    return (
      <section>
        <label>{this.props.currency}</label>
        <input className='currentInput'
               type="text"
               onChange={this.handleChange}
               value={this.props.amount}
        />
      </section>
    )
  }
}

export default CurrencyInput;

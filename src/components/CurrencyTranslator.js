import React, { Component } from 'react';
import CurrencyInput from "./CurrencyInput";

class CurrencyTranslator extends Component {

  constructor(props) {
    super(props);
    this.state = {
      current: 0,
    }
  }

  handlerChangeUSD(num) {
    if (Number.isNaN(num)) {
      return;
    }
    this.setState({
      current: num,
    })
  }

  handlerChangeCNY(num) {
    if (Number.isNaN(num / 6)) {
      return;
    }
    this.setState({
      current: num / 6
    });
  }

  render() {
    return (
      <div>
        <CurrencyInput  handlerChange={this.handlerChangeUSD.bind(this)} currency="USD"
                       amount={(this.state.current * 1).toFixed(2)}/>
        <CurrencyInput  handlerChange={this.handlerChangeCNY.bind(this)} currency="CNY"
                       amount={(this.state.current * 6).toFixed(2)}/>
      </div>
    )
  }

}

export default CurrencyTranslator;
